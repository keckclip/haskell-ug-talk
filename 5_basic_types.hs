-----------------------------------------------------------
-- In Haskell, everything has a type

module Main where
 
 -- variable types
 x1 :: Integer         -- (::) => "is of type"  
 x1 = 5                -- x1 = 5.01 gives an error

 x2 :: Double
 x2 = 5.01
 
 -- function type signatures
 square :: Integer -> Integer
 square x = x * x      -- try square x2 
 
 isTrue :: Bool -> Bool
 isTrue a = a == True
 -- try :t undefined

 -- type inference (eg list comprehension)
 square' x = x * x     -- try square' x1|x2
 x3 = True             -- try isTrue x3
 xs1 = [1,2,3]         -- lists are homogenous

 -- list comprehension
 squares xs2 = [x * x | x <- xs2]
 rightTriangles = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2] 
 boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

 tup = (square,xs1,x3) -- tuples can be heterogenous

 -- main function
 main = putStrLn "type :t <var|func|list>"
