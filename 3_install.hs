-----------------------------------------------------------
-- Install Haskell

-- Win/Mac : download Haskell plattform from Haskell.org
-- Linux   : sudo apt-get install haskell-platform (Debian)

-----------------------------------------------------------
-- Run Haskell
-- The Standard Prelude: defines standard types
--                       basic functions (map, foldr)

-- Glasgow Haskell Compiler (GHC)

ghc hello.hs

-- produces an object file, interface file, executable 

./hello

-- GHCi (REPL)
ghci
Prelude>

let r = 5
let area = pi * r^2
