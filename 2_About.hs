-----------------------------------------------------------
-- About Haskell
{-
  appeared around 1990
  purely functional 
  referentially transparent
  immutability
  no side effects
  static typing
  non-strict (lazy) evaluation
-}

-----------------------------------------------------------
-- Resources / Reference
{-
  http://www.haskell.org
  http://www.seas.upenn.edu/~cis194/lectures.html
  http://learnyouahaskell.com/chapters
  http://en.wikibooks.org/wiki/Haskell/
-}

-----------------------------------------------------------
-- IDEs
{-
  Emacs
  Vim  :)
  EclipseFP plugin for Eclipse IDE
  FP Haskell Center (online REPL)
  codeworld.info (online REPL)
  Visual Studio plugin??
  ....  among a few
-}


