-----------------------------------------------------------
-- Super basic Haskell introduction (Part 1)
-- with code examples

-- by Magnus Heinz
-- Haskell User Group: 18.06.2014

-- Agenda
{-
  About Haskell / Resources / IDEs
  Installing / running Haskell
  Basics / Hello World
  Basic Types
  Functions 1: pattern matching / guards / etc
  Functions 2: where / let binding, do notation
  Importing libraries / modules
  Next time
-}

-- Please stop / correct me at any time :)
