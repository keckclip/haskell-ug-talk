-----------------------------------------------------------
-- variables, functions, lists, hello world

module Main where
 -- Haskell likes indentation

 -- variables
 -- x1 = 5
 -- x1 = 2        -- violates immutability
 x1 = 0 : x1      -- this creates an infinite list 

 -- basic functions
 square x = x * x
 add a b = a + b

 -- lists, enumerations (homogenous types)
 xs0 = []         -- empty list
 xs1 = [1,2,3]
 xs2 = [0,2..10]  -- even numbers till 10
 xs3 = [0,-1..]   -- infinite list of neg. integers
 xs4 = take 5 xs3 -- first 5 elements of xs3

 -- main function
 main = putStrLn "Hello World!"
