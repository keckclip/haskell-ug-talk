-----------------------------------------------------------
-- functions 

module Main where

 -- function definition
 square1 x = x * x    
 square3 x = result
  where result = x * x

 -- anonymous function
 -- anSquare :: Integer -> Integer -- (BOOM)
 anSquare = \x -> x * x

 -- higher order function
 squares xs = map (\x -> x * x) xs

 -- pattern matching
 factorial 0 = 1
 factorial n = n * factorial (n - 1)

 -- guards (using Boolean functions)
 isEven x  
  | x == 0 = "zero!"
  | even x = "even!"
  | otherwise = "odd!"

 -- function composition
 factSquare1 x = (anSquare . factorial) x
 factSquare2 = anSquare . factorial 
 hypothenuse1 a b = sqrt(foldr (+) 0 (map anSquare [a,b]))
 hypothenuse2 a b = sqrt $ foldr (+) 0 $ map anSquare [a,b]

 -- partially applied function (not real currying)
 add a b = a + b
 addFive = add 5       -- (add 5) b = 5 + b

 -- main function
 main = putStrLn "type :t <var|func|list>"
