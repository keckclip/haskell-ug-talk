-----------------------------------------------------------
-- importing libraries / modules 

module Main where

 -- import Data.Char        -- import complete lib
 import Data.Char(toUpper)  -- import specific functions
 import Geometry            -- import custom module 

 strToUpper :: IO ()       -- define function as IO action
 strToUpper = do
  putStrLn("Enter a string to be capitalized:")
  str <- getLine 
  print(map toUpper str)
  
 sphereVol :: IO ()        -- define function as IO action
 sphereVol = do
  putStrLn("To find the volume of a sphere, \ 
   \enter the radius:")
  str <- getLine 
  print(sphereVolume (read str))
  
 -- main function using 'do' notation for IO
 main = do
  strToUpper
  sphereVol
