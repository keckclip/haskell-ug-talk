-----------------------------------------------------------
-- more on function syntax (where and let bindings)
-- 'do' notation

module Main where

 -- function definition using 'where'
 square1 x = x * x    
 
 square2 x = result
  where { result = x * x; }

 speedLimit n
  | n < limit = "Speed up!"
  | n > limit = "Slow down!."
  | otherwise = "Just right."
  where limit = 100

 -- function definition using 'let'
 cylinder r h = 
  let sideArea = 2 * pi * r * h  
      topArea = pi * r ^2  
  in  sideArea + 2 * topArea

 test = 
  let square x = x * x 
  in  (square 2, square 2.0) 
 
 -- main function using 'do' notation for IO
--  main = do
--   putStrLn("How fast are you driving?")
--   speed <- getLine 
--   print(speedLimit (read speed))
 
 -- main function using 'do' notation for IO
 main = putStrLn("How fast are you driving?") >>
  getLine >>= \speed -> print(speedLimit (read speed))
 
